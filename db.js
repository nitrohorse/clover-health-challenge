'use strict'

const mysql = require('mysql')
const config = require('./config')
const log = console.log

class DB {
  constructor () {
    log('Opening DB connection')
    this.connection = mysql.createConnection(config.database)
    this.connection.connect()
  }

  async executeSQL (sql, values) {
    return new Promise(resolve => {
      this.connection.query(sql, [values], () => resolve())
    })
  }

  getBulkInsertQuery (tableName, spec) {
    let sql = `insert into ${tableName} (`

    spec.forEach(columnDetail => {
      const columnName = columnDetail['column name']
      sql += `${columnName}, `
    })

    return sql.slice(0, -2) + ') values ?'
  }

  getCreateTableQuery (tableName, spec) {
    // CREATE TABLE testformat1 (name TEXT(10), valid BOOLEAN, count INTEGER(3))
    let sql = `create table if not exists ${tableName} (`

    spec.forEach(columnDetail => {
      const columnName = columnDetail['column name']
      const columnWidth = Number(columnDetail['width'])
      const columnDataType = columnDetail['datatype']

      sql += `${columnName} ${columnDataType}`

      if (columnDataType !== 'BOOLEAN') {
        sql += `(${columnWidth})`
      }

      sql += ', '
    })

    return sql.slice(0, -2) + ')'
  }

  createTable(tableName, spec) {
    const createTableSQL = this.getCreateTableQuery(tableName, spec)
    log('Creating table:', createTableSQL)
    return this.executeSQL(createTableSQL)
  }

  bulkInsert(tableName, spec, values) {
    const bulkInsertSQL = this.getBulkInsertQuery(tableName, spec)
    log('Bulk inserting:', bulkInsertSQL)
    log('values:', values)
    return this.executeSQL(bulkInsertSQL, values)
  }

  endConnection () {
    log('Closing DB connection')
    this.connection.end()
  }
}

module.exports = DB