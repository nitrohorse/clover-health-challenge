'use strict'

const csv = require('csvtojson')
const globby = require('globby')
const readline = require('readline')
const fs = require('fs')
const DB = require('./db')

const log = console.log

const readSpecFile = async (specFilePath) => {
  const jsonArray = []

  return new Promise(resolve => {
    csv()
    .fromFile(specFilePath)
    .on('json', json => {
      jsonArray.push(json)
    })
    .on('end', error => {
      resolve(jsonArray)
    })
  })
}

const getSpecFileName = specFilePath => {
  return specFilePath.substring(specFilePath.lastIndexOf('/') + 1, specFilePath.indexOf('.csv'))
}

const getAssociatedDataFilePaths = (specFileName, dataFilesPaths) => {
  return dataFilesPaths.filter(dataFilePath => {
    if (dataFilePath.includes(specFileName)) {
      return dataFilePath
    }
  })
}

const castToDataType = (columnValue, datatype) => {
  switch (datatype) {
    case 'BOOLEAN':
    case 'INTEGER':
      columnValue = Number(columnValue)
      break
    default:
      break
  }
  return columnValue
}

const readDataFile = async (spec, dataFilePath) => {
  const dataFileInterface = readline.createInterface({
    input: fs.createReadStream(dataFilePath)
  })

  const dataArray = []

  return new Promise(resolve => {
    dataFileInterface.on('line', line => {
      const values = []

      let charIndex = 0
      spec.forEach(columnDetail => {
        const columnName = columnDetail['column name']
        const columnWidth = Number(columnDetail['width'])
        const columnDataType = columnDetail['datatype']

        // read line char by char
        const endIndex = charIndex + columnWidth
        let columnValue = line.substring(charIndex, endIndex)
        charIndex = endIndex

        columnValue = columnValue.trim()

        columnValue = castToDataType(columnValue, columnDataType)
        values.push(columnValue)
      })
      dataArray.push(values)
    })

    dataFileInterface.on('close', () => {
      resolve(dataArray)
    })
  })
}

const parseFiles = async (db) => {
  try {
    const specFilesPaths = await globby('./specs/*.csv')
    const dataFilesPaths = await globby('./data/*.txt')

    log('Parsing spec files...')
    for (let specFilePath of specFilesPaths) {
      const spec = await readSpecFile(specFilePath)
      const tableName = getSpecFileName(specFilePath)
      const associatedDataFilePaths = getAssociatedDataFilePaths(tableName, dataFilesPaths)
      await db.createTable(tableName, spec)

      log('Parsing associated data files...')
      for (let dataFilePath of associatedDataFilePaths) {
        const values = await readDataFile(spec, dataFilePath)
        await db.bulkInsert(tableName, spec, values)
      }
    }
    log('Done!')
  } catch (error) {
    log('Error:', error)
  }
}

(async () => {
  try {
    const db = new DB()
    await parseFiles(db)
    db.endConnection()
  } catch (error) {
    log('Error:', error)
  }
})()