## Solution
* Login to MySQL instance: `mysql -u root -p`
* Create new database: `create database clover_health;`
* Set database: `use clover_health;`
* Show all databases: `show databases;`
* Run (steps below): `yarn start`
* Show all tables: `show tables;`
* Show records for table, testformat1: `select * from testformat1;`
```
+-----------+-------+-------+
| name      | valid | count |
+-----------+-------+-------+
| Foonyor   |     1 |     1 |
| Barzane   |     0 |   -12 |
| Quuxitude |     1 |   103 |
+-----------+-------+-------+
```
* Cleanup:
  * `drop table testformat1;`
  * `drop database clover_health;`

## Run
* Install tools
  * [Node.js + npm](https://nodejs.org/en/)
  * [Yarn](https://yarnpkg.com/en/)
* Install dependencies
  * `yarn`
* Start
  * `yarn start`